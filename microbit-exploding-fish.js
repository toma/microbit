input.onButtonPressed(Button.A, function () {
    if ("fish" == shape) {
        shape = ""
        pointsA += 1
        basic.showLeds(`
            . . . . .
            . # . . .
            # # # . .
            . # . . .
            . . . . .
            `)
        basic.pause(100)
    } else if ("bomb" == shape) {
        shape = ""
        pointsA += -2
        basic.showLeds(`
            . . . . .
            . . . . .
            # # # . .
            . . . . .
            . . . . .
            `)
        basic.pause(100)
    }
    if (pointsA >= winningScore) {
        gameOver = true
        winText = "A wins"
    }
})
input.onButtonPressed(Button.B, function () {
    if ("fish" == shape) {
        shape = ""
        pointsB += 1
        basic.showLeds(`
            . . . . .
            . . . # .
            . . # # #
            . . . # .
            . . . . .
            `)
        basic.pause(100)
    } else if ("bomb" == shape) {
        shape = ""
        pointsB += -2
        basic.showLeds(`
            . . . . .
            . . . . .
            . . # # #
            . . . . .
            . . . . .
            `)
        basic.pause(100)
    }
    if (pointsB >= winningScore) {
        gameOver = true
        winText = "B wins"
    }
})
let shape = ""
let winText = ""
let gameOver = false
let winningScore = 0
let pointsB = 0
let pointsA = 0
pointsA = 0
pointsB = 0
winningScore = 5
gameOver = false
winText = ""
basic.forever(function () {
    while (gameOver == true) {
        basic.showString("" + winText)
        basic.pause(100)
    }
    shape = ""
    basic.pause(Math.randomRange(1000, 3000))
    if (Math.randomBoolean()) {
        shape = "fish"
        basic.showLeds(`
            . . . . .
            . # # . #
            # . . # #
            # . . # #
            . # # . #
            `)
        basic.pause(500)
        basic.clearScreen()
    } else {
        shape = "bomb"
        basic.showLeds(`
            . . . # .
            . # # . .
            # . . # .
            # . . # .
            . # # . .
            `)
        basic.pause(500)
        basic.clearScreen()
    }
    shape = ""
})

