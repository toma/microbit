input.onButtonPressed(Button.A, function () {
    mode = "GRAPH"
})
input.onButtonPressed(Button.B, function () {
    mode = "ABC"
})
input.onButtonPressed(Button.AB, function () {
    radio.sendString(msg)
})
radio.onReceivedString(function (receivedString) {
    basic.showString(receivedString)
    basic.pause(200)
    basic.clearScreen()
})
let pinMax = 0
let pin = 0
let base = 0
let msg = ""
let mode = ""
let abc = " ABCDEFGHIJKLMNOPQRSTUVWXYZ"
mode = "ABC"
basic.forever(function () {
    base = 1.004
    pin = pins.analogReadPin(AnalogPin.P1)
    pin = base ** pin
    pinMax = base ** 1023
    if ("GRAPH" == mode) {
        led.plotBarGraph(
        pin,
        pinMax
        )
    } else if ("ABC" == mode) {
        msg = abc.charAt(Math.map(pin, 0, pinMax, 0, abc.length + 4))
        basic.showString(msg)
    } else if ("123" == mode) {
        basic.showString("" + pin)
    }
})

